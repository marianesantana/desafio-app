import { Component, OnInit } from '@angular/core';
import { LancamentosService } from './service/lancamentos.service'
import { Lancamento } from './interface/lancamento.interface';
import { Categoria } from '../categorias/categoria.interface';



@Component({
  selector: 'app-lancamentos',
  templateUrl: './lancamentos.component.html',
  styleUrls: ['./lancamentos.component.css']
})
export class LancamentosComponent implements OnInit {
  lancamento = {} as Lancamento;
  lancamentos: Lancamento[];

  categoria = {} as Categoria;
  categorias: Categoria[];
  porMes: [string, string][];
  porCategoria: [string, string][];


  constructor(private lancamentosService: LancamentosService) {

  }

  ngOnInit(): void {
    this.getLancamentos();
    this.getAllList();


  }
  getLancamentos() {
    return this.lancamentosService.getAllLancamentos().subscribe((lancamentos: Lancamento[]) => {
     this.porMes = Object.entries(lancamentos.reduce((acc, lancamento) => {
        if (!acc[lancamento.mes_lancamento]){
          acc[lancamento.mes_lancamento] = 0;
        }
        acc[lancamento.mes_lancamento] = acc[lancamento.mes_lancamento] + lancamento.valor;
        console.log(acc[lancamento.mes_lancamento]);
        return acc;
      }, {}));
    });
  }
  // getCategories() {
  //   return this.lancamentosService.getCategoria().subscribe((categorias: Categoria[]) => {
  //     this.porCategoria = Object.entries(categorias.reduce((acc, categoria, lancamento) => {
  //        if (!acc[categoria.nome]){
  //          acc[categoria.nome] = 0;
  //        }
  //        acc[categoria.nome] = acc[categoria.nome] + lancamento.valor;
  //        console.log(acc[categoria.nome]);
  //        return acc;
  //      }, {}));
  //    });
  // }
  getAllList() {
    return this.lancamentosService.getAllLancamentos().subscribe((lancamentos: Lancamento[]) => {
      this.lancamentos = lancamentos;
      return lancamentos;
    });
  }
}

