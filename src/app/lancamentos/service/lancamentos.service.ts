import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Lancamento } from '../interface/lancamento.interface';
import { Categoria } from 'src/app/categorias/categoria.interface';


@Injectable({
  providedIn: 'root'
})
export class LancamentosService {
  constructor(private http: HttpClient) { }

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };

  urlApi = 'https://desafio-it-server.herokuapp.com';

  getCategoria(): Observable<Categoria[]>{
    return this.http.get<any>(this.urlApi + '/categorias')

  }

   getAllLancamentos(): Observable<Lancamento[]> {
       return this.http.get<Lancamento[]>(this.urlApi + '/lancamentos');
   }
}