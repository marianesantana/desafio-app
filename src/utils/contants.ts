const MONTH = {
    janeiro: 'JANEIRO',
    fevereiro: 'FEVEREIRO',
    marco: 'MARÇO',
    abril: 'ABRIL',
    maio: 'MAIO',
    junho: 'JUNHO',
    julho: 'JULHO',
    agosto: 'AGOSTO',
    setembro: 'SETEMBRO',
    outubro: 'OUTUBRO',
    novembro: 'NOVEMBRO',
    dezembro: 'DEZEMBRO'
}
